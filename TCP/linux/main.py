from flask import Flask
from flask import request
import pyautogui

app = Flask(__name__)

@app.route('/left_click', methods=['GET'])
def left_click():
    pyautogui.keyDown('z')  # Hold down the "z" key
    return 'Left Click Sent'

@app.route('/left_release', methods=['GET'])
def left_click_release():
    pyautogui.keyUp('z')  # Release the "z" key
    return 'Left Click Released'

@app.route('/right_click', methods=['GET'])
def right_click():
    pyautogui.keyDown('x')  # Hold down the "x" key
    return 'Right Click Sent'

@app.route('/right_release', methods=['GET'])
def right_click_release():
    pyautogui.keyUp('x')  # Release the "x" key
    return 'Right Click Released'

if __name__ == '__main__':
    app.run(host='0.0.0.0')
