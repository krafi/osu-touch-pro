from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
import requests

class MouseControlApp(App):
    pc_ip = None
    session = None

    def build(self):
        layout = BoxLayout(orientation='vertical')

        # Create IP input field
        ip_input = TextInput(hint_text='Enter PC IP address', text='192.168.0.', multiline=False, input_type='number')
        layout.add_widget(ip_input)

        # Create connect button
        connect_button = Button(text='Connect', on_press=lambda _: self.set_pc_ip(ip_input.text))
        layout.add_widget(connect_button)

        return layout

    def set_pc_ip(self, ip):
        self.pc_ip = ip
        print(f"PC IP set to: {self.pc_ip}")
        self.session = requests.Session()
        self.start_mouse_control()

    def start_mouse_control(self):
        layout = BoxLayout(orientation='vertical')

        # Create left button
        left_button = Button(text='Left Click', on_press=self.send_left_click, on_release=self.send_left_release)
        layout.add_widget(left_button)

        # Create right button
        right_button = Button(text='Right Click', on_press=self.send_right_click, on_release=self.send_right_release)
        layout.add_widget(right_button)

        self.root.clear_widgets()  # Clear previous layout
        self.root.add_widget(layout)  # Add new layout

    def send_request(self, action):
        if not self.pc_ip:
            print("PC IP is not set.")
            return

        url = f'http://{self.pc_ip}:5000/{action}'
        print(f"Sending {action} request to: {url}")
        try:
            response = self.session.get(url)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.handle_request_error(e)

    def send_left_click(self, *args):
        self.send_request('left_click')

    def send_left_release(self, *args):
        self.send_request('left_release')

    def send_right_click(self, *args):
        self.send_request('right_click')

    def send_right_release(self, *args):
        self.send_request('right_release')

    def handle_request_error(self, error):
        print(f"Request failed with error: {error}")

if __name__ == '__main__':
    MouseControlApp().run()
