import socket
import subprocess
import pyautogui

HOST = '0.0.0.0'
PORT = 5000

def handle_request(data, address, udp_socket):
    command = data.decode()
    if command == 'left_click':
        pyautogui.keyDown('z')  # Hold down the "z" key
        response = 'Left Click Sent'

    elif command == 'left_release':
        pyautogui.keyUp('z')  # Release the "z" key
        response = 'Left Click Released'

    elif command == 'right_click':
        pyautogui.keyDown('x')  # Hold down the "x" key
        response = 'Right Click Sent'

    elif command == 'right_release':
        pyautogui.keyUp('x')  # Release the "x" key
        response = 'Right Click Released'

    else:
        response = 'Invalid command'

    udp_socket.sendto(response.encode(), address)

def start_udp_server():
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_socket.bind((HOST, PORT))

    print(f"UDP server started on {HOST}:{PORT}")

    while True:
        data, address = udp_socket.recvfrom(1024)
        handle_request(data, address, udp_socket)

if __name__ == '__main__':
    start_udp_server()
