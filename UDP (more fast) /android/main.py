from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
import socket


class MouseControlApp(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.pc_ip = None
        self.udp_socket = None
        self.udp_port = 5000

    def build(self):
        root = BoxLayout(orientation='vertical')
        layout = BoxLayout(orientation='vertical')

        # Create IP input field
        ip_input = TextInput(hint_text='Enter PC IP address', text='192.168.0.', multiline=False, input_type='number')
        layout.add_widget(ip_input)

        # Create connect button
        connect_button = Button(text='Connect')
        connect_button.bind(on_press=lambda _: self.set_pc_ip(ip_input.text))
        layout.add_widget(connect_button)

        root.add_widget(layout)
        return root

    def set_pc_ip(self, ip):
        self.pc_ip = ip
        print(f"PC IP set to: {self.pc_ip}")
        self.start_mouse_control()

    def start_mouse_control(self):
        root = BoxLayout(orientation='vertical')

        # Create left button
        left_button = Button(text='Left Click')
        left_button.bind(on_press=self.send_left_click, on_release=self.send_left_release)
        root.add_widget(left_button)

        # Create right button
        right_button = Button(text='Right Click')
        right_button.bind(on_press=self.send_right_click, on_release=self.send_right_release)
        root.add_widget(right_button)

        self.root.clear_widgets()
        self.root.add_widget(root)

        # Create UDP socket
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def send_left_click(self, *args):
        self.send_udp_message('left_click')

    def send_left_release(self, *args):
        self.send_udp_message('left_release')

    def send_right_click(self, *args):
        self.send_udp_message('right_click')

    def send_right_release(self, *args):
        self.send_udp_message('right_release')

    def handle_request_error(self, error):
        print(f"Request failed with error: {error}")

    def send_udp_message(self, message):
        try:
            self.udp_socket.sendto(message.encode(), (self.pc_ip, self.udp_port))
        except socket.error as e:
            print(f"Error sending UDP message: {e}")

    def on_stop(self):
        if self.udp_socket:
            self.udp_socket.close()


if __name__ == '__main__':
    MouseControlApp().run()
